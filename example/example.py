#!/usr/bin/env python

import stompy, json, re

def http(cl, frame):
    inp = json.loads(frame.body)
    if inp['url'].find('sockjs.min.js') >= 0:
        cl.put(
            json.dumps(
                dict(
                    id=inp['id'],
                    code=200,
                    headers={'Content-Type':'application/javascript'},
                    content=open('sockjs-0.3.4.min.js').read())),
            '/amq/queue/http_out',
            )
    elif inp['url'].find('angular.min.js') >= 0:
        cl.put(
            json.dumps(
                dict(
                    id=inp['id'],
                    code=200,
                    headers={'Content-Type':'application/javascript'},
                    content=open('angular.min.js').read())),
            '/amq/queue/http_out',
            )
    else:
        cl.put(
            json.dumps(
                dict(
                    id=inp['id'],
                    code=200,
                    headers={'Content-Type':'text/html; charset=UTF-8'},
                    content=index_page())),
            '/amq/queue/http_out',
            )

def index_page():
    return '''
        <html data-ng-app="test" id="data-ng-app" lang="ru" xmlns:data-ng="http://angularjs.org">
          <head>
            <script type="text/javascript" src="sockjs.min.js"></script>
            <script type="text/javascript" src="angular.min.js"></script>
            <script type="text/javascript">
              var app = angular.module("test", []);
              app.controller("ctrl", [
                '$scope', function($scope) {
                  window.$scope = $scope;
                  $scope.inp = '';
                }]);
              var sj = new SockJS('http://' + window.location.host + '/comet');
              sj.onopen = function() {
                $scope.$watch('inp', function(nv, ov) {
                  sj.send(JSON.stringify({data:nv,to:'comet_mess'}));
                });
                sj.onmessage = function(msg) {
                  $scope.$apply(function() {
                    $scope.out = JSON.parse(msg.data).data;
                  });
                };
              };
            </script>
          </head>
          <body data-ng-controller="ctrl">
            <input data-ng-model="inp"/><br/>
            upper case:<br/>
            {{out}}
          </body>
        </html>
        '''

def comet_mess(cl, frame):
    inp = json.loads(frame.body)
    cl.put(json.dumps(dict(data=inp['data'].upper())), '/amq/queue/'+inp['from'])

def main():
    cl = stompy.simple.Client()
    cl.connect()
    handlers = {
        '/queue/http_in': http,
        '/queue/comet_mess': comet_mess,
        }
    for qname, handl in handlers.iteritems():
        cl.subscribe(qname)
    while True:
        frame = cl.get()
        handlers[frame.headers['destination']](cl, frame)

if __name__ == '__main__': main()
