#!/usr/bin/env python
#coding:utf-8

import tornado.ioloop
import pika, json
import sockjs.tornado
from pika.adapters.tornado_connection import TornadoConnection

def main(sockjs_path='/comet', http_in='http_in', http_out='http_out', amq_user='guest', amq_pass='guest', amq_host='127.0.0.1', amq_port=5672, amq_vhost='/', server_port=8080, server_host='127.0.0.1'):
    def amqp(cb):
        def on_connected(connection):
            def on_channel_open(channel):
                def subscribe(queue=None, callback=(lambda *_:_), consumer_tag=None, durable=True, exclusive=False, ready=lambda *_:_):
                    tmp = {}
                    def _subscribe(q):
                        def _callback(_a0, _a1, _a2, text):
                            callback(text)
                        tmp['queue'] = q.method.queue
                        tmp['consumer_tag'] = channel.basic_consume(
                            **dict(
                                (dict(consumer_tag=consumer_tag) if consumer_tag else {}),
                                consumer_callback=_callback,
                                queue=q.method.queue,
                                no_ack=True,
                                )
                              )
                        ready(tmp['queue'], lambda: channel.basic_cancel(consumer_tag=tmp['consumer_tag']))
                    channel.queue_declare(
                        **dict(
                            (dict(queue=queue) if queue else {}),
                            durable=durable,
                            auto_delete=True,
                            exclusive=exclusive,
                            callback=_subscribe,
                            )
                         )
                    return lambda: tmp['queue'], lambda: channel.basic_cancel(consumer_tag=tmp['consumer_tag'])
                def send(queue, data):
                    channel.basic_publish(
                        exchange='',
                        routing_key=queue,
                        body=data,
                        properties=pika.BasicProperties(
                            content_type="text/json",
                            delivery_mode=2, # 2 - durable
                            )
                        )
                cb(send, subscribe)
            connection.channel(on_channel_open)
        connection = TornadoConnection(
            pika.ConnectionParameters(
                host=amq_host,
                port=amq_port,
                virtual_host=amq_vhost,
                credentials=pika.PlainCredentials(amq_user, amq_pass)
                ),
            on_open_callback=on_connected
            )


    def start(send, subs):
        class SJC(sockjs.tornado.SockJSConnection):
            def __init__(self, *x, **y):
                sockjs.tornado.SockJSConnection.__init__(self, *x, **y)
            def on_open(self, info):
                self._q = []
                def on_pika_message(data):
                    self.send(data)
                def _subscribed(qname, unsub):
                    self.queue = qname
                    self.unsub = unsub
                    for i in self._q:
                        self._on_message(i)
                    self._q = None
                subs(callback=on_pika_message, ready=_subscribed, consumer_tag='ctag_'+str(id(self)))
            def on_message(self, message):
                if self._q is None:
                    self._on_message(message)
                else:
                    self._q.append(message)
            def _on_message(self, message):
                data = json.loads(message)
                data['from'] = self.queue
                send(data['to'], json.dumps(data))
            def message(self, data):
                self.send(json.dumps(data))
            def on_close(self):
                self.unsub()

        requests = {}
        def web_callback(jsondata):
            data = json.loads(jsondata)
            handler = requests.pop(data['id'])
            handler.set_status(data.get('code', 200))
            for k, v in data.get('headers', {}).items():
                handler.set_header(k, v)
            handler.write(data.get('content', ''))
            handler.finish()
        subs(http_out, web_callback)
        class HTTPHandler(tornado.web.RequestHandler):
            @tornado.web.asynchronous
            def reqw(self):
                requests[id(self)] = self
                send(http_in, json.dumps(dict(
                            id=id(self),
                            url='%s://%s%s' % (self.request.protocol, self.request.host, self.request.uri),
                            method=self.request.method,
                            headers=self.request.headers,
                            content=self.request.body,
                            remote_ip=self.request.remote_ip,
                            )))
            get=reqw
            post=reqw
            put=reqw
            delete=reqw
            head=reqw
            options=reqw

        app = tornado.web.Application(sockjs.tornado.SockJSRouter(SJC, sockjs_path).urls + [(r"/.*", HTTPHandler),])
        app.listen(server_port, server_host)

    amqp(start)

    ioloop = tornado.ioloop.IOLoop.instance()
    ioloop.start()

if __name__ == '__main__': main()
